#### compute the covariance, variances, and correlation between alphas #######
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra
using Statistics

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

###### argument variables ####
const repl = ARGS[1]
const gen = parse(Int16, ARGS[2])
const domval = ARGS[3]
const epival = ARGS[4]
const epimodel = ARGS[5]
const breed1 = ARGS[6]
const breed2 = ARGS[7]
const outfile = ARGS[8]

########
const alphaf1 = string(repl,"/",domval,"_",epival,"_",epimodel,"/alphas_",breed1,"_",gen,"_",repl,".dat")
const alphaf2 = string(repl,"/",domval,"_",epival,"_",epimodel,"/alphas_",breed2,"_",gen,"_",repl,".dat")

### read data file ###
alpha1 = convert(Array, readdlm(alphaf1, Float64)[:,1])
alpha2 = convert(Array, readdlm(alphaf2, Float64)[:,1])

#### covariance, variances and correlation ####
cv = cov(alpha1, alpha2)
v1 = var(alpha1)
v2 = var(alpha2)
rg = cor(alpha1, alpha2)

### write results ####
f=open("$repl/$outfile","a")
	write(f, "$domval $epival $epimodel $gen $repl $rg $cv $v1 $v2 \n")
close(f)
