#### compute the mean difference in allele frequencies between two lines ####
#### allele frequencies must be present in a file ####
using DelimitedFiles
using Statistics

if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

##### parameters ####
const repl = ARGS[1]
const gen1 = parse(Int16, ARGS[2])
const gen2 = parse(Int16, ARGS[3])
const breed1 = ARGS[4]
const breed2 = ARGS[5]
const domval = ARGS[6]
const epival = ARGS[7]
const epimodel = ARGS[8]
const outfile = ARGS[9]

##### read qtl loci ####
selloci = convert(Array, readdlm("qtlloci_"*repl*".dat", Int64)[:,1])

##### read allele frequencies ####
p1 = convert(Array, readdlm(string(repl,"/allelefreq_",breed1,"_",gen1,".dat"), Float64)[:,1])
p2 = convert(Array, readdlm(string(repl,"/allelefreq_",breed2,"_",gen2,".dat"), Float64)[:,1])

### filter qtl ####
p1=p1[selloci]
p2=p2[selloci]

##### compute mean difference in allele frequencies ####
meandiff = mean(broadcast(abs,(p1 .- p2)))

### write results ####
f=open("$repl/$outfile","a")
	write(f, "$domval $epival $epimodel $breed1 $breed2 $gen1 $gen2 $repl $meandiff \n")
close(f)
