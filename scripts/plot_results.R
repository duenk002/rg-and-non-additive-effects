#### rpc table ####
require(reshape2)
require(plyr)
require(ggplot2)
require(wesanderson)

#### get summary statistics ####
summaryPD<-function (data = NULL, measurevar, groupvars = NULL, na.rm = TRUE, 
                     conf.interval = 0.95, .drop = TRUE){
  length2 <- function(x, na.rm = FALSE) {
    if (na.rm) 
      sum(!is.na(x))
    else length(x)
  }
  datac <- ddply(data, groupvars, .drop = .drop, .fun = function(xx, col, na.rm) {
    c(N = length2(xx[, col], na.rm = na.rm), 
      mean = mean(xx[,col], na.rm = na.rm), 
      sd = sd(xx[, col], na.rm = na.rm),
      q0 = unname(min(xx[,col], na.rm=na.rm)),
      q1 = unname(quantile(xx[,col], na.rm=na.rm, 0.75)),
      q3 = unname(quantile(xx[,col], na.rm=na.rm, 0.25)),
      q4 = unname(max(xx[,col], na.rm=na.rm)) )
  }, measurevar, na.rm)
  datac$se <- datac$sd/sqrt(datac$N)
  ciMult <- qt(conf.interval/2 + 0.5, datac$N - 1)
  datac$ci <- datac$se * ciMult
  colnames(datac)[colnames(datac) == "mean"] = measurevar
  return(datac)
}

##### theme for plots ############
theme_set(
  theme_bw(base_size = 10) +
    theme(
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.background = element_blank(),
      panel.border = element_rect(fill = NA, colour="black", size=0.3),
      strip.background = element_rect(fill = NA, colour="black", size=0.3),
      axis.text.x = element_text(size=10),
      axis.ticks.x = element_blank(),
      legend.position="top",
      legend.margin=margin(0, unit='cm'),
      legend.key.height=unit(0,"line"),
      legend.spacing = unit(0,"line"),
      legend.text.align = 0
    )
)

colpalette = wes_palette(n=5, name="Darjeeling1")

cbbPalette <- c("#6BAED6", "#2171B5", "#08306B")
cbbPalette2 <- c("grey","#6BAED6", "#2171B5", "#08306B")

### function for more colours ###
gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}

######################
setwd("D:/Projects/05 Armidale/HPC")
options(stringsAsFactors=F)

outfolder<-"rg_5"

### rpc files
infile<-"rg_5/rg_drift_5.log"
infile2<-"rg_5/rg_5.log"
infile_bv<-"rg_5/rg_bvs_drift_5.log"
infile_bv2<-"rg_5/rg_bvs_5.log"

########
dir.create(outfolder, showWarnings=F)

##################################
rpc<-read.table(infile, header=F)
rpc2<-read.table(infile2, header=F)
rpcbv<-read.table(infile_bv, header=F)
rpcbv2<-read.table(infile_bv2, header=F)

rpc$selection<-"drift"
rpc2$selection<-"sel"
rpcbv$selection<-"drift"
rpcbv2$selection<-"sel"

rpc<-rbind(rpc, rpc2)
rpcbv<-rbind(rpcbv, rpcbv2)

colnames(rpc)<-c('domval','epival','epimodel','gen','repl','rpc','VA1','VA2','VD1','VD2','VI1','VI2','selection')
colnames(rpcbv)<-c('domval','epival','epimodel','gen','repl','rpcbv','cv','v1','v2','selection')

rpc<-merge(rpc,rpcbv,by=c('domval','epival','epimodel','gen','repl','selection'), all=T)

#### 
setwd(outfolder)

### create variable magnitude ################################
rpc[rpc$domval != "none", 'scenario']<-'dominance'
rpc[rpc$domval == "none", 'scenario']<-rpc[rpc$domval == "none",'epimodel']
rpc[rpc$scenario == "none", 'scenario']<-'none'
rpc[rpc$scenario == "none", 'magnitude']<-'none'

rpc$magnitude<-ifelse(rpc$domval == "none", rpc$epival, rpc$domval)

#### change the words used for magnitudes ####
rpc[rpc$magnitude == "low", 'magnitude']<-"small"
rpc[rpc$magnitude == "medium", 'magnitude']<-"intermediate"
rpc[rpc$magnitude == "high", 'magnitude']<-"large"

#### compute total genetic variance ####
rpc$VT1<-rpc$VA1+rpc$VD1+rpc$VI1
rpc$VT2<-rpc$VA2+rpc$VD2+rpc$VI2

#### compute ratios ####################
rpc$h21<-rpc$VA1/rpc$VT1
rpc$d21<-rpc$VD1/rpc$VT1
rpc$i21<-rpc$VI1/rpc$VT1

rpc$h22<-rpc$VA2/rpc$VT2
rpc$d22<-rpc$VD2/rpc$VT2
rpc$i22<-rpc$VI2/rpc$VT2
########################################

#### compute loss of genetic variance #####
getstart<-function(xx){
  return(data.frame(startA=xx[xx$gen == 1, 'v1'],
                    startD=xx[xx$gen == 1, 'VD1'],
                    startI=xx[xx$gen == 1, 'VI1']))
}

starttable<-ddply(rpc, .(magnitude,repl,scenario, selection), getstart)
rpc<-merge(rpc, starttable, by=c('repl','scenario','magnitude','selection'), all.x=T)
rpc$aloss<-rpc$v1/rpc$startA
rpc$dloss<-rpc$VD1/rpc$startD
rpc$iloss<-rpc$VI1/rpc$startI

#### set factors #######################
rpc$scenario<-factor(rpc$scenario, levels=c("none","dominance","aa","dd","complementary","interaction"),
                     labels=c("A","D",expression(E[AA]),expression(E[DD]),expression(E[C]),expression(E[M])))
rpc$magnitude<-factor(rpc$magnitude, levels=c("none","small","intermediate","large"))
rpc$magnitude<-droplevels(rpc$magnitude)

rpc$selection<-factor(rpc$selection, levels=c("drift","sel"), labels=c("drift","selection"))

##### plot rg versus initial epistatic variance ######
rpc$startNA<-rpc$startD+rpc$startI
flt<-rpc$gen == 50 & rpc$scenario !="A" & rpc$selection == "selection"

ggplot(rpc[flt,], aes(y=rpcbv, x=startNA, col=magnitude))+
  geom_point()+
  scale_colour_manual(values=cbbPalette)+
  guides(col=guide_legend(nrow=1,byrow=TRUE))+
  labs(x="inital non-additive variance", y=expression(r[g]), col="Magnitude")+
  facet_wrap(~scenario, ncol=2, labeller = label_parsed, scales="free")+
  ggsave("rg_epivar.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(rpc[flt,], aes(y=rpcbv, x=startNA, col=scenario))+
  geom_point()+
  scale_colour_manual(values=colpalette, labels=c("D",expression(E[AA]),expression(E[DD]), expression(E[C]), expression(E[M])))+
  guides(col=guide_legend(nrow=1,byrow=TRUE))+
  labs(x="inital non-additive variance", y=expression(r[g]), col="Genetic model")+
  facet_wrap(~magnitude, ncol=3, scales="free_x")+
  ggsave("rg_epivar2.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

#### summary stats ###########
rpc2<-summaryPD(rpc, 'rpc', groupvars=c('scenario','magnitude','gen','selection'))
rpcbv2<-summaryPD(rpc, 'rpcbv', groupvars=c('scenario','magnitude','gen','selection'))

#### RPC ############
write.table(rpc[,c('gen','scenario','magnitude','repl','selection','rpcbv')], "rpc_all.dat", col.names=T, row.names=F, quote=F)

#### TABLES #########
rpcmean<-dcast(rpc2, scenario+magnitude+selection~gen, value.var='rpc')
rpcsd<-dcast(rpc2, scenario+magnitude+selection~gen, value.var='sd')

rpcmean$value<-"mean"
rpcsd$value<-"sd"

rpctable<-rbind(rpcmean, rpcsd)
rpctable<-rpctable[order(rpctable$scenario, rpctable$magnitude, rpctable$value),]

write.table(rpctable, "rpc.dat", col.names=T, row.names=F, quote=F)

#### for breeding values #####
rpcmean<-dcast(rpcbv2, scenario+magnitude+selection~gen, value.var='rpcbv')
rpcsd<-dcast(rpcbv2, scenario+magnitude+selection~gen, value.var='sd')

rpcmean$value<-"mean"
rpcsd$value<-"sd"

rpctable<-rbind(rpcmean, rpcsd)
rpctable<-rpctable[order(rpctable$scenario, rpctable$magnitude, rpctable$value),]

write.table(rpctable, "rpc_bv.dat", col.names=T, row.names=F, quote=F)

##### BASE SCENARIO #######################################
base<-ddply(rpc, .(gen,scenario,magnitude,selection), function(xx)data.frame(rpcbv=mean(xx$rpcbv)))
base<-dcast(base, selection+magnitude+gen~scenario, value.var='rpcbv',FUN=mean)
write.table(base, "base.dat", col.names=T, row.names=F, quote=F)

flt<-rpc$magnitude == "small" & rpc$gen == 50 & rpc$selection == "drift"

ggplot(rpc[flt,], aes(y=rpcbv, x=scenario))+
  geom_boxplot(size=0.4, outlier.size = 0.5, fatten=1.5, aes(middle = mean(rpcbv)))+
  scale_y_continuous(limits=c(0.0,1.0))+
  ylab(expression(r[g]))+
  ggsave("base.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

########### BOXPLOT SELECTION VERSUS NO SELECTION #########
seldrift<-ddply(rpc, .(gen,scenario,magnitude,selection), function(xx)data.frame(rpcbv=mean(xx$rpcbv)))
seldrift<-dcast(seldrift, scenario+magnitude+gen~selection, value.var='rpcbv',FUN=mean)
write.table(seldrift, "selection.dat", col.names=T, row.names=F, quote=F)

flt<-rpc$magnitude == "small" & rpc$gen == 50 & rpc$scenario != "A"

ggplot(rpc[flt,], aes(y=rpcbv, x=selection, col=selection))+
  geom_boxplot(size=0.4, outlier.size = 0.5, fatten=1.5, aes(middle = mean(rpcbv)))+
  facet_wrap(~scenario, ncol=5, labeller = label_parsed)+
  scale_y_continuous(limits=c(0.0,1.0))+
  ylab(expression(r[g]))+
  labs(col="")+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())+
  ggsave("selection.pdf", width=170, height=60, dpi=4*72, units="mm", scale=1)

flt<-rpc$magnitude == "small" & rpc$gen == 50 & rpc$scenario != "A" & rpc$selection == "selection"

ggplot(rpc[flt,], aes(y=rpcbv, x=selection, col=selection))+
  geom_boxplot(size=0.4, outlier.size = 0.5, fatten=1.5, aes(middle = mean(rpcbv)))+
  facet_wrap(~scenario, ncol=5, labeller = label_parsed)+
  scale_y_continuous(limits=c(0.0,1.0))+
  ylab(expression(r[g]))+
  labs(col="")+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())+
  ggsave("selection2.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

############ BOXPLOT MAGNITUDES ##########################
magnitude<-ddply(rpc, .(gen,scenario,magnitude,selection), function(xx)data.frame(rpcbv=mean(xx$rpcbv)))
magnitude<-dcast(magnitude, scenario+selection+gen~magnitude, value.var='rpcbv',FUN=mean)
write.table(magnitude, "magnitude.dat", col.names=T, row.names=F, quote=F)

flt<-rpc$gen == 50 & rpc$selection == "selection" & rpc$scenario != "A"

ggplot(rpc[flt,], aes(y=rpcbv, x=magnitude, col=magnitude))+
  geom_boxplot(size=0.4, outlier.size = 0.5, fatten=1.5, aes(middle = mean(rpcbv)))+
  facet_wrap(~scenario, ncol=5, labeller = label_parsed)+
  scale_y_continuous(limits=c(0.0,1.0))+
  ylab(expression(r[g]))+
  labs(col="")+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())+
  scale_colour_manual(values=cbbPalette)+
  ggsave("magnitude.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

flt<-rpc$gen == 50 & rpc$selection == "drift" & rpc$scenario != "A"

ggplot(rpc[flt,], aes(y=rpcbv, x=magnitude, col=magnitude))+
  geom_boxplot(size=0.4, outlier.size = 0.5, fatten=1.5, aes(middle = mean(rpcbv)))+
  scale_y_continuous(limits=c(0.0,1.0))+
  facet_wrap(~scenario, ncol=5, labeller = label_parsed)+
  ylab(expression(r[g]))+
  labs(col="")+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())+
  scale_colour_manual(values=cbbPalette)+
  ggsave("magnitude_drift.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

############# BOXPLOT GENERATIONS ###########################
generations<-ddply(rpc, .(gen,scenario,magnitude,selection), function(xx)data.frame(rpcbv=mean(xx$rpcbv)))
generations<-dcast(generations, scenario+selection+magnitude~gen, value.var='rpcbv',FUN=mean)
write.table(generations, "generations.dat", col.names=T, row.names=F, quote=F)

flt<-rpc$selection == "selection" & rpc$scenario != "A" & rpc$magnitude == "small"

ggplot(rpc[flt,], aes(y=rpcbv, x=as.factor(gen), col=as.factor(gen)))+
  geom_boxplot(size=0.4, outlier.size = 0.5, fatten=1.5, aes(middle = mean(rpcbv)))+
  facet_wrap(~scenario, ncol=5, labeller = label_parsed)+
  scale_y_continuous(limits=c(-0.2,1.0))+
  ylab(expression(r[g]))+
  labs(col="Generation")+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())+
  ggsave("generation.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

###### LINE PLOT WITH ALL VARIABLES ###########################
ggplot(rpc2[rpc2$selection == "selection",], aes(y=rpc, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=rpc-sd, ymax=rpc+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y=expression(r[g]), col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, labeller = label_parsed)+
  scale_x_continuous(limits=c(0,50))+
  scale_y_continuous(limits=c(-0.1,1.1))+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("rg.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(rpcbv2[rpcbv2$selection == "selection",], aes(y=rpcbv, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=rpcbv-sd, ymax=rpcbv+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y=expression(r[g]), col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, labeller = label_parsed)+
  scale_x_continuous(limits=c(0,50))+
  scale_y_continuous(limits=c(-0.1,1.1))+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("rg_bv.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

flt<-rpcbv2$selection == "selection" & rpcbv2$scenario != "A" & rpcbv2$magnitude %in% c("small","large")

ggplot(rpcbv2[flt,], aes(y=rpcbv, x=gen, col=scenario))+
  geom_errorbar(aes(ymin=rpcbv-sd, ymax=rpcbv+sd), size=0.1, width=0.5)+
  geom_line()+
  scale_colour_manual(values=colpalette, labels=c("D",expression(E[AA]),expression(E[DD]), expression(E[C]), expression(E[M])))+
  guides(col=guide_legend(nrow=1,byrow=TRUE))+
  labs(x="generation", y=expression(r[g]), col="Genetic model")+
  scale_y_continuous(limits=c(-0.1,1.05))+
  scale_x_continuous(limits=c(0,50))+
  facet_grid(~magnitude)+
  ggsave("generation2.pdf", width=170, height=85, dpi=4*72, units="mm", scale=1)

###### check linear model between cor alphas and cor bvs #########
##### scatter plot ####################
ggplot(rpc[rpc$selection == "selection",], aes(y=rpcbv, x=rpc))+
  geom_point()+
  labs(x="Correlation of alphas", y="Correlation of breeding values")+
  geom_abline(intercept = 0, slope=1)+
  facet_grid(scenario ~ magnitude, labeller=label_parsed)+
  coord_fixed(xlim=c(0.0,1.0), ylim=c(0.0,1.0))+
  ggsave("rg_det_vs_bv.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

func2 <- function(xx){  
  ll<-lm(xx$rpcbv ~ 0 + xx$rpc)
  bias<-mean(xx$rpcbv, na.rm=T) - mean(xx$rpc, na.rm=T)
  return(data.frame(bias = bias,
                    reg_coef = ll$coefficients[[1]],
                    r = cor(xx$rpcbv, xx$rpc)))
}

reg_rg_bv<-ddply(rpc[rpc$gen>0,], .(gen, scenario,magnitude), func2)
write.table(reg_rg_bv, "reg_rg_bv.dat", col.names=T, quote=F, row.names=F)

################# VARIANCE COMPONENTS ######################################
############################################################################

va1<-summaryPD(rpc, 'VA1', groupvars=c('scenario','magnitude','gen','selection'))
va2<-summaryPD(rpc, 'VA2', groupvars=c('scenario','magnitude','gen','selection'))
vd1<-summaryPD(rpc, 'VD1', groupvars=c('scenario','magnitude','gen','selection'))
vd2<-summaryPD(rpc, 'VD2', groupvars=c('scenario','magnitude','gen','selection'))
vi1<-summaryPD(rpc, 'VI1', groupvars=c('scenario','magnitude','gen','selection'))
vi2<-summaryPD(rpc, 'VI2', groupvars=c('scenario','magnitude','gen','selection'))
vt1<-summaryPD(rpc, 'VT1', groupvars=c('scenario','magnitude','gen','selection'))
vt2<-summaryPD(rpc, 'VT2', groupvars=c('scenario','magnitude','gen','selection'))

valoss<-summaryPD(rpc, 'aloss', groupvars=c('scenario','magnitude','gen','selection'))
vdloss<-summaryPD(rpc, 'dloss', groupvars=c('scenario','magnitude','gen','selection'))
viloss<-summaryPD(rpc, 'iloss', groupvars=c('scenario','magnitude','gen','selection'))


### for ratios ###############
func <- function(xx){  return(data.frame(h21 = mean(xx$h21),
                                         sdh = sd(xx$h21),
                                         d21 = mean(xx$d21),
                                         sdd = sd(xx$d21),
                                         i21 = mean(xx$i21),
                                         sdi = sd(xx$h21),
                                         h22 = mean(xx$h22),
                                         d22 = mean(xx$d22),
                                         i22 = mean(xx$i22))) }
vartable<-ddply(rpc, .(gen,scenario,magnitude,selection), func)

######### for population 1 #############################
measvar<-c('h21','d21','i21')
vartable3<-melt(vartable, id.vars=c('scenario', 'magnitude', 'gen','selection'), measure.vars=measvar, variable.name='type')
vartable3<-vartable3[vartable3$scenario != "A" & vartable3$selection == "selection",]

##### line plot ####################
ggplot(vartable3, aes(y=value, x=gen, col=factor(type)))+
  geom_line()+
  labs(x="generation", y="Proportion", col="Component")+
  facet_grid(scenario ~ magnitude, labeller = label_parsed)+
  scale_color_discrete(labels=c("A","D","I"))+
  ggsave("varcomp.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

######### for population 2 #############################
measvar<-c('h22','d22','i22')
vartable3<-melt(vartable, id.vars=c('scenario', 'magnitude', 'gen','selection'), measure.vars=measvar, variable.name='type')
vartable3<-vartable3[vartable3$scenario != "A" & vartable3$selection == "selection",]

##### line plot ####################
ggplot(vartable3, aes(y=value, x=gen, col=factor(type)))+
  geom_line()+
  labs(x="generation", y="Proportion", col="Component")+
  facet_grid(scenario ~ magnitude, labeller = label_parsed)+
  scale_color_discrete(labels=c("A","D","I"))+
  ggsave("varcomp2.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

#### plot total genetic variance ######
ggplot(vt1[vt1$selection == "selection",], aes(y=VT1, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=VT1-sd, ymax=VT1+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y="VT", col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, scales="free", labeller = label_parsed)+
  annotate(geom='line', x=vt1[vt1$scenario=="none", 'gen'], y=vt1[vt1$scenario=="none",'VT1'], col="black", lty=2)+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("vt1.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(vt2[vt2$selection == "selection",], aes(y=VT2, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=VT2-sd, ymax=VT2+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y="VT", col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, scales="free", labeller = label_parsed)+
  annotate(geom='line', x=vt2[vt2$scenario=="none", 'gen'], y=vt2[vt2$scenario=="none",'VT2'], col="black", lty=2)+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("vt2.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(va1[va1$selection == "selection",], aes(y=VA1, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=VA1-sd, ymax=VA1+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y="VA", col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, scales="free", labeller = label_parsed)+
  annotate(geom='line', x=va1[va1$scenario=="none", 'gen'], y=va1[va1$scenario=="none",'VA1'], col="black", lty=2)+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("va1.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(va2[va2$selection == "selection" & va2$magnitude == "intermediate",], aes(y=VA2, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=VA2-sd, ymax=VA2+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y="VA", col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, scales="free", labeller = label_parsed)+
  annotate(geom='line', x=va2[va2$scenario=="none", 'gen'], y=va2[va2$scenario=="none",'VA2'], col="black", lty=2)+
  scale_colour_manual(values=cbbPalette)+
  ggsave("va2.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

flt<-valoss$scenario=="A" & valoss$selection == "selection"

ggplot(valoss[valoss$selection == "selection",], aes(y=aloss, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=aloss-sd, ymax=aloss+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y=expression(VA/VA[g1]), col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2, labeller = label_parsed)+
  annotate(geom='line', x=valoss[flt, 'gen'], y=valoss[flt,'aloss'], col="black", lty=2)+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("valoss.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(valoss[valoss$magnitude == "low" & valoss$selection == "selection",], aes(y=aloss, x=gen, col=scenario))+
  geom_errorbar(aes(ymin=aloss-sd, ymax=aloss+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y=expression(VA/VA[g1]), col="Genetic model")+
  annotate(geom='line', x=valoss[flt, 'gen'], y=valoss[flt,'aloss'], col="black", lty=2)+
  scale_colour_manual(values=colpalette, labels=c("D",expression(E[AA]),expression(E[DD]), expression(E[C]), expression(E[M])))+
  guides(col=guide_legend(nrow=2,byrow=TRUE))+
  ggsave("valoss2.pdf", width=85, height=85, dpi=4*72, units="mm", scale=1)

ggplot(vdloss[vdloss$selection == "selection",], aes(y=dloss, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=dloss-sd, ymax=dloss+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y=expression(VD/VD[g1]), col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2)+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("vdloss.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

ggplot(viloss[viloss$selection == "selection",], aes(y=iloss, x=gen, col=magnitude))+
  geom_errorbar(aes(ymin=iloss-sd, ymax=iloss+sd), size=0.1, width=0.5)+
  geom_line()+
  labs(x="generation", y=expression(VI/V[g1]), col="Magnitude")+
  facet_wrap(~ scenario, ncol = 2)+
  scale_colour_manual(values=cbbPalette2)+
  ggsave("viloss.pdf", width=170, height=170, dpi=4*72, units="mm", scale=1)

### make table ####
h21<-dcast(vartable, scenario+magnitude+selection~gen, value.var='h21', mean)
d21<-dcast(vartable, scenario+magnitude+selection~gen, value.var='d21', mean)
i21<-dcast(vartable, scenario+magnitude+selection~gen, value.var='i21', mean)

sdh<-dcast(vartable, scenario+magnitude+selection~gen, value.var='sdh', mean)

h22<-dcast(vartable, scenario+magnitude+selection~gen, value.var='h22', mean)
d22<-dcast(vartable, scenario+magnitude+selection~gen, value.var='d22', mean)
i22<-dcast(vartable, scenario+magnitude+selection~gen, value.var='i22', mean)

vartable2<-cbind(h21[c(1,2,3,8)],d21[8],i21[8],h22[8],d22[8],i22[8])
write.table(vartable2, "variance_table.dat", col.names=T, row.names=F, quote=F)

write.table(vartable, "variance_table_full.dat", col.names=T, row.names=F, quote=F)

