## function to compute LD for all combinations of loci ######
function compLD(genotypes1, genotypes2, lmap, af1, af2)
	
	n1 = size(lmap)[1]
	ncombs = Int((n1*(n1-1))/2)
	
	dist = zeros(Float32, ncombs)
	ld1 = zeros(Float32, ncombs)
	ld2 = zeros(Float32, ncombs)
	
	ids = zeros(Int64, ncombs, 2)
	
	println("Computing pairwise LD")
	
	### create distance matrix
	wc = 1
	for i in 2:n1
		for j in 1:(i-1)
				
			dist[wc] = abs(lmap[i,:pos] - lmap[j,:pos])
			
			if (af1[i] == 0.0 && af1[j] .== 0.0) || (af1[i] == 1.0 && af1[j] == 1.0)
				ld1[wc] = 1.0
			elseif (af1[i] == 0.0 && af1[j] .== 1.0) || (af1[i] == 1.0 && af1[j] .== 0.0)
				ld1[wc] = -1.0
			else
				ld1[wc] = cor(genotypes1[:,i], genotypes1[:,j])
			end
			
			if (af2[i] == 0.0 && af2[j] .== 0.0) || (af2[i] == 1.0 && af2[j] == 1.0)
				ld2[wc] = 1.0
			elseif (af2[i] == 0.0 && af2[j] .== 1.0) || (af2[i] == 1.0 && af2[j] .== 0.0)
				ld2[wc] = -1.0
			else
				ld2[wc] = cor(genotypes2[:,i], genotypes2[:,j])
			end
			
			ids[wc,:] = [i j]
			wc += 1
			
		end
	end
	return ld1, ld2, ids, dist
end
