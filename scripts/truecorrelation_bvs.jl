##### compute genetic values of all animals in all generations, using frequencies of generation 1 ####
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra
using Statistics

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_readgeno.jl")
include("$scriptfp/f_allelefrequencies.jl")
include("$scriptfp/f_noia_gfreq.jl")
include("$scriptfp/f_gfreq.jl")

###### argument variables ####
const repl = ARGS[1]
const gen = parse(Int16, ARGS[2])
const domval = ARGS[3]
const epival = ARGS[4]
const epimodel = ARGS[5]
const breed1 = ARGS[6]
const breed2 = ARGS[7]
const outfile = ARGS[8]

#### other variables ####
const gfile1 = string(breed1,"_mrk_qtl_",repl,".txt")
const datfile1 = string(breed1,"_data_",repl,"_reformat.dat")

### read data file ###
dat1 = CSV.read(datfile1, delim=' ')

### filter on generation ###
dat1 = dat1[dat1[:G] .== gen,:]

### selected ids ####
selids1 = convert(Array, dat1[:Progeny])

### read genotypes 
geno1, geno_phase1, nloc1 = read_genotypes(gfile1, selids1, true, true)

GC.gc()

### get loci places ###
qtlloci = convert(Array, readdlm("qtlloci_"*repl*".dat", Int64)[:,1])

#### filter genotype matrix ####
geno1 = geno1[:,qtlloci]

### make NOIA matrix ######
gfreq1 = get_gfreq(geno1)
noia1, noia_dom1 = make_noia(geno1, gfreq1)

### get effects ####
dr = string(repl,"/",domval,"_",epival,"_",epimodel)
alpha1 = convert(Array, readdlm(string(dr,"/alphas_",breed1,"_",gen,"_",repl,".dat"), ' ', Float64))[:,1]
alpha2 = convert(Array, readdlm(string(dr,"/alphas_",breed2,"_",gen,"_",repl,".dat"), ' ', Float64))[:,1]

#### compute breeding values, dominance deviations etc. ####
println("Computing values")

simbvA1 = noia1 * alpha1
simbvA2 = noia1 * alpha2

#### bvs
bvs = [simbvA1 simbvA2]

### correlation ####
rg = cor(bvs[:,1], bvs[:,2])[1]
cv = cov(bvs[:,1], bvs[:,2])[1]
v1 = var(bvs[:,1])[1]
v2 = var(bvs[:,2])[1]

#################################
### write results ###
f=open("$repl/$outfile","a")
	write(f, "$domval $epival $epimodel $gen $repl $rg $cv $v1 $v2 \n")
close(f)
################################