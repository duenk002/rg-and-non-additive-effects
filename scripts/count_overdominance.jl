##### count the number of overdominant loci for all dominance replicates #####
using DataFrames
using DelimitedFiles
using Statistics

cd("/lustre/nobackup/WUR/ABGC/duenk002/armidale/effects_5")

magnitudes = ["low"; "medium"; "high"]
reps = collect(1:1:20)

repchar = map(x->lpad(string(x),3,string(0)), reps)

mean_overdom = zeros(Float64, size(magnitudes)[1])

for j in 1:size(magnitudes)[1]

	m = magnitudes[j]

	n_overdom = zeros(Int64, size(repchar)[1])
	
	for i in 1:size(repchar)[1]
	
		r = repchar[i]
		a = readdlm("a_"*r*".dat", Float64)[:,1]
		d = readdlm("d_"*m*"_"*r*".dat", Float64)[:,1]
		
		a = map(x->abs(x), a)
		d = map(x->abs(x), d)
		
		n_overdom[i] = count(d .> a)[1]
		
	end
	
	mean_overdom[j] = mean(n_overdom)
	
end

fr_mean_overdom = mean_overdom ./ 500