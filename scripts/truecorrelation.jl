########## compute the genetic correlation between traits in two breeds (A and B)
#################################################################################
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_epistasis.jl")
include("$scriptfp/f_geteffects.jl")
include("$scriptfp/f_getvariances.jl")
include("$scriptfp/f_allelefrequencies.jl")

###### argument variables ####
const repl = ARGS[1]
const gen = parse(Int16, ARGS[2])
const domval = ARGS[3]
const epival = ARGS[4]
const epimodel = ARGS[5]
const breed1 = ARGS[6]
const breed2 = ARGS[7]
const outfile = ARGS[8]
const efolder = ARGS[9]

repchar = repl

#### qtl loci positions ####
# qtlloci = convert(Array, readdlm(efolder*"/qtlloci_"*repchar*".dat", Int64)[:,1])
qtlloci = convert(Array, readdlm("qtlloci_"*repchar*".dat", Int64)[:,1])
nqtl = size(qtlloci)[1]

### get effects ####
a = convert(Array, readdlm(efolder*"/a_"*repchar*".dat",' ', Float64))
d = convert(Array, readdlm(efolder*"/d_"*domval*"_"*repchar*".dat",' ', Float64))
epi_coef = convert(Array, readdlm(efolder*"/e_"*epival*"_"*repchar*".dat",' ', Float64))

### epi combinations ###
epi_combinations = convert(Array, readdlm(efolder*"/epi_combinations_"*repchar*".dat",' ', Int64))
epi_coef = epi_coef[1:size(epi_combinations)[1]]

##
nreqqtl = size(a)[1]

###### get allele frequencies ####
p1 = readdlm(string(repchar,"/allelefreq_",breed1,"_",gen,".dat"), Float64)[:,1]
p2 = readdlm(string(repchar,"/allelefreq_",breed2,"_",gen,".dat"), Float64)[:,1]

qtlp1 = p1[qtlloci] ## pb line 1
qtlp2 = p2[qtlloci] ## pb line 2

##### set fixed allele frequencies to an extreme frequency, so that the algorithm in epi() still works for all loci #####
qtlp1[ qtlp1 .== 1.0 ] .= 0.99999
qtlp1[ qtlp1 .== 0.0 ] .= 0.00001
qtlp2[ qtlp2 .== 1.0 ] .= 0.99999
qtlp2[ qtlp2 .== 0.0 ] .= 0.00001

### compute genotype frequencies ######
gfreq1 = gf(qtlp1,qtlp1)
gfreq2 = gf(qtlp2,qtlp2)

###### compute effects ####
epi_vars1, epi_effects1 = epi(gfreq1, qtlp1, epi_coef, a, epimodel, epi_combinations)
epi_vars2, epi_effects2 = epi(gfreq2, qtlp2, epi_coef, a, epimodel, epi_combinations)

alphas1, ds1 = get_effects(a,d,qtlp1,epi_combinations, epi_effects1)
alphas2, ds2 = get_effects(a,d,qtlp2,epi_combinations, epi_effects2)

#### compute rpc ################
rg = cor(alphas1, alphas2)

#### compute variances ####
Va1, Vd1 = get_vars(alphas1, ds1, gfreq1)
Va2, Vd2 = get_vars(alphas2, ds2, gfreq2)

V_A1 = sum(Va1)
V_A2 = sum(Va2)

V_D1 = sum(Vd1)
V_D2 = sum(Vd2)

V_I1 = sum(epi_vars1[:,[5 6 7]])
V_I2 = sum(epi_vars2[:,[5 6 7]])

######

### write results ####
f=open("$repchar/$outfile","a")
	write(f, "$domval $epival $epimodel $gen $repl $rg $V_A1 $V_A2 $V_D1 $V_D2 $V_I1 $V_I2 \n")
close(f)

### write alphas ####
dr = string(repchar,"/",domval,"_",epival,"_",epimodel)
if !isdir(dr)
	mkdir(dr)
end

writedlm(string(dr,"/alphas_", breed1,"_",gen,"_",repchar,".dat"), alphas1, ' ')
writedlm(string(dr,"/alphas_", breed2,"_",gen,"_",repchar,".dat"), alphas2, ' ')

########################