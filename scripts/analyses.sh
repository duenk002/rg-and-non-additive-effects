#!/bin/bash

#### Pascal Duenk, 2019-07-29 #######
#### this shell script shows how to run all analyses to get the results for the paper ####

#### PROGRAMS NEEDED ######

#### QMSIM 2.0 (20 Apr 2019)
#### R version 3.4.3 (2017-11-30)
#### Julia Version 0.7.0 (2018-08-08 06:46 UTC)
#### inquire about package versions

###########################################################
##### sample the functional effects for each replicate ####
qtlfold="effects_5"
nqtl=500

mkdir -p $qtlfold

##### for each replicate #####
for rep in $(seq 1 50) 
do

  cd basefolder
  repl=$(printf "%03d" $rep) 

  declare -a dom=("none","low","medium","high")
  declare -a epi=("none","low","medium","high") 

  ### sample additive ####
  julia scripts/sample_a.jl $repl $nqtl $qtlfold

  ### sample dominance effects ###
  for d in "${dom[@]}"
  do
    julia scripts/sample_d.jl $repl $d $qtlfold
  done

  ### sample epistatic coefficients ####
  for e in "${epi[@]}"
  do
    julia scripts/sample_e.jl $repl $e 5 $qtlfold
  done

  ### sample 5 interactions per locus ####
  julia scripts/sample_epicombinations.jl $repl $nqtl 5 $qtlfold
done

##### DRIFT SCENARIOS ##########

### run simulation #####
QMSim scripts/simulation_drift2.prm -o ;

#### for every replicate, and every genetic model, run all computations
for rep in $(seq 1 50) 
do 
  
  ### set replicate name
  repl=$(printf "%03d" $rep)
  
  ### go to folder 
  cd basefolder/r_simulation_drift2

  #### format data ####
  dataA="BreedA_data_${repl}.txt"
  dataB="BreedB_data_${repl}.txt"

  Rscript ../scripts/format_data.R $dataA $dataB

  ### sample loci ####
  julia ../scripts/sample_loci.jl $repl $nqtl

  #### set generations #####
  declare -a gens=(1 2 3 4 5 10 15 20 25 30 35 40 45 50)

  ## compute allele frequencies for each generation ####
  for g in "${gens[@]}" 
  do
    julia ../scripts/ldphase.jl BreedA BreedB $g $g $repl nold 
  done
  ######

  while read LINE
  do 
    domval=$(echo "$LINE" | cut -f1 -d' ')
    epival=$(echo "$LINE" | cut -f2 -d' ')
    epimod=$(echo "$LINE" | cut -f3 -d' ')
        
    #### compute all other parameters ###
    for g in "${gens[@]}"
    do
      julia ../scripts/truecorrelation.jl $repl $g $domval $epival $epimod BreedA BreedB rg_5.log $qtlfold
      julia ../scripts/truecorrelation_bvs.jl $repl $g $domval $epival $epimod BreedA BreedB rg_bv_5.log
      julia ../scripts/truevarcovcor.jl $repl $g $domval $epival $epimod BreedA BreedB varcovcor_5.log
      julia ../scripts/meandiffp.jl $repl $g $g BreedA BreedB $domval $epival $epimod meandiff_5.log
    done
    
  done < /basefolder/scenario_table.dat

done

#### SELECTION SCENARIOS ########

#### run simulation for none (additive model), to sample loci to be used for all other genetic models ####
### variables ####
domval="none"
epival="none"
epimod="none"

echo $domval $epival $epimod

#### run simulation ####
cd basefolder
nm="${domval}_${epival}_${epimod}"

mkdir -p $nm
cd $nm

### remove temp file if the analysis was done previously ##
rm rep.tmp -rf

### copy parameter file ###
fnm="${nm}.prm"	
cp ../scripts/simulation2.prm $fnm 

### run simulations ####
julia ../scripts/create_ebvsh.jl $domval $epival $epimod BreedA 50 20 0.5 $qtlfold NA
QMSim $fnm -o 

#### copy sampled loci to qtlfold #####
for rep in $(seq 1 50)
do
  repl=$(printf "%03d" $rep)
  cp "qtlloci_${repl}.dat" /basefolder/$qtlfold
done

##### run simulation for all other genetic models ######
for rep in $(seq 1 16)
do

  cd basefolder
  ### read line ####
  ln=$(sed "${rep}q;d" /basefolder/scenario_table.dat)

  ### extract variables ####
  domval=$(echo $ln | cut -f1 -d' ')
  epival=$(echo $ln | cut -f2 -d' ')
  epimod=$(echo $ln | cut -f3 -d' ')

  echo $domval $epival $epimod

  ### remove and make folder ###
  nm="${domval}_${epival}_${epimod}"
  fnm="${domval}_${epival}_${epimod}_5"

  rm $fnm -rf   #### remove previously created folders ####
  mkdir -p $fnm
  cd $fnm

  ### copy parameter file ###
  prm="${nm}.prm"	
  cp ../scripts/simulation2.prm $prm

  echo "done"

  ### run simulations ####
  julia ../scripts/create_ebvsh.jl $domval $epival $epimod BreedA 50 20 $qtlfold $qtlfold
  QMSim $prm -o 
  
done

###### Run computations of values and rg etc. for all genetic models ##########
for rep in $(seq 1 20)
do

  repl=$(printf "%03d" $rep) 

  declare -a gens=(1 2 3 4 5 10 15 20 25 30 35 40 45 50) 

  cd basefolder

  while read LINE
  do 
    domval=$(echo "$LINE" | cut -f1 -d' ')
    epival=$(echo "$LINE" | cut -f2 -d' ')
    epimod=$(echo "$LINE" | cut -f3 -d' ')
    
    nm="${domval}_${epival}_${epimod}"
    nm2="${domval}_${epival}_${epimod}_5"
    
    cd $nm2
    
    fold="r_${nm}"
    cd $fold 
    
    #### remove folder from previous analyses? ###
    rm $repl -rf
    
    #### format data ####
    dataA="BreedA_data_${repl}.txt"
    dataB="BreedB_data_${repl}.txt"

    Rscript ../../scripts/format_data.R $dataA $dataB 
    
    #### compute genetic trends ####
    julia ../../scripts/genetic_trend.jl $repl $domval $epival $epimod 1 50 8 BreedA $qtlfold

    #### compute all other parameters ###
    for g in "${gens[@]}"
    do
      julia ../../scripts/ldphase.jl BreedA BreedB $g $g $repl nold 
      julia ../../scripts/truecorrelation.jl $repl $g $domval $epival $epimod BreedA BreedB rg_5.log $qtlfold
      julia ../../scripts/truecorrelation_bvs.jl $repl $g $domval $epival $epimod BreedA BreedB rg_bv_5.log
      julia ../../scripts/truecorrelation_bvs.jl $repl $g $domval $epival $epimod BreedB BreedA rg_bv_5_b.log     #### correlation between breeding values in population B
      julia ../../scripts/truevarcovcor.jl $repl $g $domval $epival $epimod BreedA BreedB varcovcor_5.log
      julia ../../scripts/meandiffp.jl $repl $g $g BreedA BreedB $domval $epival $epimod meandiff_5.log
    done
    
  done < /basefolder/scenario_table.dat
done

###### collect all the results for drift ###########
cd basefolder

file1="../../rg_bvs_drift_5.log"
file2="../../rg_drift_5.log"
file3="../../varcovcor_drift_5.log"
file4="../../meandiffp_drift_5.log"

cd r_simulation_drift2

for r in $(seq 1 50)
do
	repl=$(printf "%03d" $r)
	
	echo $repl 
	
	cd $repl
  
	cat rg_bv_5.log >> $file1
	cat rg_5.log >> $file2 
  cat varcovcor_5.log >> $file3
  cat meandiff_5.log >> $file4
  
  cd ..
  
done 

###### collect all the results for selection #####
cd basefolder

file1="../../../rg_5.log"
file2="../../../rg_bvs_5.log"
file3="../../../varcovcor_5.log"
file4="../../../meandiffp_5.log"

while read LINE
do 
	domval=$(echo "$LINE" | cut -f1 -d' ')
	epival=$(echo "$LINE" | cut -f2 -d' ')
	epimod=$(echo "$LINE" | cut -f3 -d' ')
	
	nm="${domval}_${epival}_${epimod}"
	nm2="${domval}_${epival}_${epimod}_5"
	
	cd $nm2
	
	fold="r_${nm}"
	cd $fold 
	
	echo $nm 
	
	for r in $(seq 1 20)
	do
		repl=$(printf "%03d" $r)
		echo $repl 
		cd $repl
		cat rg_5.log >> $file1 
		cat rg_bv_5_b.log >> $file2 
    cat varcovcor_5.log >> $file3
    cat meandiff_5.log >> $file4
		cd .. 
	done 
	
	cd .. 
	cd ..
	
done < /basefolder/scenario_table.dat 

########  PLOT ALL THE RESULTS #####
######## NOTE THAT THESE SCRIPTS MAY NOT WORK DIRECTLY BECAUSE OF DIFFERENCES IN PACKAGE VERSIONS ETC. ###
######## BE SURE TO CHANGE SOME VARIABLES IN THE R FILES (FOLDERS ETC.) #####
######## ALSO, COPY THE FILES WITH RESULTS TO a folder named rg_5 and afs_5 ##########

cd basefolder

### rg and variance plots ####
Rscript scripts/plot_results.R

### plot mean difference in allele frequencies ####
Rscript scripts/plot_meandiff.R

### plot other allele frequency plots #####
Rscript scripts/plot_allelefrequencies.R

### plot relationship between allele frequency differences and rg ####
Rscript scripts/plot_afs_vs_rg.R

#### plot genetic trend ####
Rscript scripts/genetic_trend.R

####### OTHER ANALYSES ################

#### two locus models in discussion ###
julia scripts/epistasis_twolocus.jl
Rscript scripts/plot_twolocus.R

#### pedigree inbreeding to compute Ne in the selected population ###
Rscript scripts/ped_inbreeding.R

#### fraction of loci with overdominance ###
julia scripts/count_overdominance.jl

#### very large non-additive effects ####
#### these scenarios can be replicated by multiplying all effects in the files of folder effects_5 by 100.
Rscript scripts/effects_vl.R
#### then use these effects to rerun some of the scenarios

#### 100 interactions per locus with epistasis #####
#### these scenarios can be replicated by sampling 100 effects instead of 5 (line x this file), and 100 locus pairs (line x this file).
#### use the resulting files to rerun some of the scenarios 